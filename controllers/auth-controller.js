const bcrypt = require('bcrypt')
const saltRounds = 10
const {User} = require('../models/userModel')
const jwt = require('jsonwebtoken')
const {JWT_SECRET} = require('../config')

module.exports.login = async (req, res) => {
    const {email, password} = req.body
    if (!email || !password ) {
        return res.status(400).json({message: `'Enter your credentials!`})
    }

    const user = await User.findOne({email})

    if (!user ) {
        return res.status(400).json({message: `No user with name - '${email}' found`})
    }
    if (!(await bcrypt.compare(password, user.password))) {
        return res.status(400).json({message: `Wrong password`})
    }

    const token = jwt.sign({username: user.email, _id: user._id}, JWT_SECRET)
    res.status(200).json({
        jwt_token: token
    })
}
module.exports.registration = async (req, res) => {
    const {email, password, role} = req.body
    if (!email || !password || !role ) {
        return res.status(400).json({message: 'Enter all credentials'})
    }
    if ( await User.findOne({ email })) {
        return res.status(400).json({ message: 'User is already exist!' })
    }
    const user = new User({
        email,
        role,
        password: await bcrypt.hash(password, saltRounds)
    })

    await user.save()

    res.header().json({message: "Profile created successfully"})
}
module.exports.forgotPass = async (req, res) => {
    const {email} = req.body
    if (!email) {
        return res.status(400).json({message: `'Enter your credentials!`})
    }

    const user = await User.findOne({email})

    if (!user ) {
        return res.status(400).json({message: `No user with name - '${email}' found`})
    }
    res.status(200).json({
        message: 'New password sent to your email address'
    })
}
