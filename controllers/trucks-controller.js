const {Truck} = require('../models/truckModel')
const {User} = require('../models/userModel')
const {badRequestError} = require('../errors')

const addTruck = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const { type } = req.body

    const truck = new Truck({
        created_by: user._id,
        type: type
    })

    await truck.save()

    res.json({message: 'Truck created successfully'})
}

const getTrucks = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const trucks = await Truck.find({created_by: user._id}, {__v: 0})

    res.json({trucks});
}

const getTruckById = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const created_by = user._id
    const _id = req.params['id']


    const truck = await Truck.findOne({created_by, _id}, {__v: 0});

    if (!truck) {
        throw new badRequestError('No trucks find with your id')
    }
    req.truck = truck;

    res.json({truck: req.truck});
};

const updateTruckById = async (req, res, next) => {
    if (!req.body.type) {
        throw new badRequestError('No type of truck')
    }
    const userId = req.user._id
    const _id = req.params['id']

    const truck = await Truck.findOne({_id});

    await Truck.updateOne(truck, {type: req.body.type});

    res.json({message: 'Truck details changed successfully'});
};

const deleteTruckById = async (req, res, next) => {
    await Truck.findByIdAndDelete(req.params['id']);
    res.json({message: 'Truck deleted successfully'});
};

const assignTruckById = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})

    const truck = await Truck.findOne({created_by: user._id, _id: req.params.id}, {__v: 0});
    await Truck.updateMany({created_by: user._id}, {assigned_to: null});
    await Truck.updateOne(truck, {assigned_to: user._id});

    res.json({message: 'Truck assigned successfully'});
};



module.exports = {
    addTruck,
    getTrucks,
    getTruckById,
    updateTruckById,
    deleteTruckById,
    assignTruckById
}
