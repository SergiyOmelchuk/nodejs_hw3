const {findTrackTypeByCriteria} = require("../trucks-parameters");
const {Load} = require('../models/loadModel')
const {User} = require('../models/userModel')
const {Truck} = require('../models/truckModel')
const {badRequestError} = require('../errors')

const addLoad = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const {name, payload, pickup_address, delivery_address, dimensions} = req.body

    const load = new Load({
        created_by: user._id,
        name,
        payload,
        pickup_address,
        delivery_address,
        dimensions
    })

    await load.save()

    res.json({message: 'Load created successfully'})
}

const getLoads = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const loads = await Load.find({created_by: user._id}, {__v: 0})

    res.json({loads});
}

const getLoadById = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const created_by = user._id
    const _id = req.params['id']


    const load = await Load.findOne({created_by, _id}, {__v: 0});

    if (!load) {
        throw new badRequestError('No loads find with your id')
    }
    req.load = load;

    res.json({load: req.load});
};

const updateLoadById = async (req, res, next) => {
    const _id = req.params['id']
    const load = req.body;
    await Load.findByIdAndUpdate(_id, load)
    res.json({message: 'Load details changed successfully'});
};

const deleteLoadById = async (req, res, next) => {
    await Load.findByIdAndDelete(req.params['id']);
    res.json({message: 'Load deleted successfully'});
};

const postLoadById = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const load = await Load.findOne({created_by: user._id, _id: req.params.id}, {__v: 0});
    await Load.updateOne(load, {status: 'POSTED'});

    const truckTypes = findTrackTypeByCriteria(load.dimensions, load.payload)

    const tr = () => {
        if (truckTypes.length === 0) {
            Load.updateOne(load, {status: 'NEW'});
            return res.json({message: 'Load not posted - driver not found', driver_found: false});
        }
    }
    await tr()

    const truck = await Truck.findOne({type: {$in: truckTypes}, status: 'IS', assigned_to: {$ne: null}})
    const tru = () => {
        if (!truck) {
            Load.updateOne(load, {status: 'NEW'});
            return res.json({message: 'Load not posted - driver not found', driver_found: false});
        }
    }
    await tru()
    await Load.findOneAndUpdate({_id: load._id}, {
        status: 'ASSIGNED',
        assigned_to: truck._id
    });
    await Truck.updateOne(truck, {status: 'OL'});


    res.json({message: 'Load posted successfully', driver_found: true});
};

const getAssignLoad = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const truck = await Truck.findOne({assigned_to: user._id})
    const load = await Load.findOne({assigned_to: truck._id}, {__v: 0})

    res.json({load: load || {}});
}

const editLoadState = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const truck = await Truck.findOne({assigned_to: user._id})
    const load = await Load.findOne({assigned_to: truck._id}, {__v: 0})
    const st = ['En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery']
    const newState = st[st.indexOf(load.state) + 1]
    await Load.findOneAndUpdate({_id: load._id}, {
        state: newState,
        status: newState === 'Arrived to delivery' ? 'SHIPPED' : load.status,
        assigned_to: newState === 'Arrived to delivery' ? null : load.assigned_to
    });
    console.log(newState)
    if (newState === 'Arrived to delivery') {
        console.log(newState)
        await Truck.findOneAndUpdate({_id: truck._id}, {status: 'IS'});
    }

    res.json({message: `Load state changed to ${newState}`});
}

module.exports = {
    addLoad,
    getLoads,
    getLoadById,
    updateLoadById,
    deleteLoadById,
    postLoadById,
    getAssignLoad,
    editLoadState
}
