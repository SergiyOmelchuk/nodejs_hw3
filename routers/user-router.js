const express = require('express')
const {changeUserPasword} = require("../controllers/user-controller")
const {validatePasswordChange} = require("./middlewares/validate-pass-change")
const router = express.Router()

const {asyncWrapper} = require('./helpers')
const {validateToken} = require('./middlewares/validate-token')
const { getUserProfile, deleteUser } = require('../controllers/user-controller')

router.get('/me', asyncWrapper(validateToken), asyncWrapper(getUserProfile))
router.delete('/me', asyncWrapper(validateToken), asyncWrapper(deleteUser))
router.patch('/me/password', asyncWrapper(validateToken), asyncWrapper(validatePasswordChange), asyncWrapper(changeUserPasword))


module.exports = router
