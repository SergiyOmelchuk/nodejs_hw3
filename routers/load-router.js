const express = require('express')
const {validateIsStatusNew} = require("./middlewares/validate-load-status");

const router = express.Router()
const {validateAddLoadFields} = require("./middlewares/validation-middlewares")
const {validateIsTruckAssigned} = require("./middlewares/validate-assigned-trucks")
const {validateIsShipper, validateIsDriver} = require("./middlewares/validate-user-role")
const {asyncWrapper} = require('./helpers')
const {validateToken} = require('./middlewares/validate-token')
const { getLoads, addLoad, getLoadById, deleteLoadById, updateLoadById, postLoadById, getAssignLoad, editLoadState} = require('../controllers/loads-controller')

router.post('/', asyncWrapper(validateToken), asyncWrapper(validateIsShipper), asyncWrapper(validateAddLoadFields),
    asyncWrapper(addLoad))
router.get('/', asyncWrapper(validateToken), asyncWrapper(validateIsShipper), asyncWrapper(getLoads))
router.get('/active', asyncWrapper(validateToken), asyncWrapper(validateIsDriver), asyncWrapper(getAssignLoad))
router.patch('/active/state', asyncWrapper(validateToken), asyncWrapper(validateIsDriver), asyncWrapper(editLoadState))
router.get('/:id', asyncWrapper(validateToken), asyncWrapper(validateIsShipper), asyncWrapper(getLoadById))
router.put('/:id', asyncWrapper(validateToken), asyncWrapper(validateIsShipper), asyncWrapper(validateIsStatusNew), asyncWrapper(validateAddLoadFields), asyncWrapper(updateLoadById))
router.delete('/:id', asyncWrapper(validateToken), asyncWrapper(validateIsShipper), asyncWrapper(validateIsStatusNew), asyncWrapper(deleteLoadById))
router.post('/:id/post', asyncWrapper(validateToken), asyncWrapper(validateIsShipper), asyncWrapper(postLoadById))



module.exports = router
