const Joi = require('joi')
const {badRequestError} = require('../../errors')

module.exports.validatePasswordChange = async (req, res, next) => {
    const schema = Joi.object({
        oldPassword: Joi.string()
            .required(),
        newPassword: Joi.string()
            .required(),
    })
    try {
        await schema.validateAsync(req.body)
    } catch (err) {
        throw new badRequestError(err.message)
    }

    next()
}
