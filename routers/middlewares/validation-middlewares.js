const Joi = require('joi')
const {badRequestError} = require('../../errors')

module.exports.validateRegistration = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string()
            .email()
            .required(),
        password: Joi.string()
            .required(),
        role: Joi.string()
            .required(),
    })

    try {
        await schema.validateAsync(req.body)
    } catch (err) {
        throw new badRequestError(err.message)
    }

    next()
}
module.exports.validateLoginFields = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string()
            .email()
            .required(),
        password: Joi.string()
            .required()
    })

    try {
        await schema.validateAsync(req.body)
    } catch (err) {
        throw new badRequestError(err.message)
    }

    next()
}
module.exports.validateForgotPassFields = async (req, res, next) => {
    const schema = Joi.object({
        email: Joi.string()
            .email()
            .required()
    })

    try {
        await schema.validateAsync(req.body)
    } catch (err) {
        throw new badRequestError(err.message)
    }

    next()
}
module.exports.validateAddTruckFields = async (req, res, next) => {
    const schema = Joi.object({
        type: Joi.string()
            .required()
    })

    try {
        await schema.validateAsync(req.body)
    } catch (err) {
        throw new badRequestError(err.message)
    }

    next()
}
module.exports.validateAddLoadFields = async (req, res, next) => {
    const schema = Joi.object({
        name: Joi.string()
            .required(),
        payload: Joi.number()
            .required(),
        pickup_address: Joi.string()
            .required(),
        delivery_address: Joi.string()
            .required(),
        dimensions: Joi.object()
            .required(),
    })

    try {
        await schema.validateAsync(req.body)
    } catch (err) {
        throw new badRequestError(err.message)
    }

    next()
}

