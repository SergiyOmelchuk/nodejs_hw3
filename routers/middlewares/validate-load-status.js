const {Load} = require("../../models/loadModel");
const {User} = require("../../models/userModel");


module.exports.validateIsStatusNew = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const created_by = user._id
    const _id = req.params['id']

    const load = await Load.findOne({created_by, _id}, {__v: 0});
    if(load.status !== 'NEW') {
        return res.status(400).json({message: `'You can not change you load with status not "NEW'`})
    }

    next()
}
