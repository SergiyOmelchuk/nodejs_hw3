const {User} = require("../../models/userModel")
module.exports.validateIsDriver = async (req, res, next) => {
    const { _id } = req.user
    const user = await User.findOne({_id})
    if(user.role !== 'DRIVER') {
        return res.status(400).json({message: `'This functionality is for Drivers only`})
    }

    next()
}
module.exports.validateIsShipper = async (req, res, next) => {
    const { _id } = req.user
    const user = await User.findOne({_id})
    if(user.role !== 'SHIPPER') {
        return res.status(400).json({message: `'This functionality is for Shippers only`})
    }

    next()
}
