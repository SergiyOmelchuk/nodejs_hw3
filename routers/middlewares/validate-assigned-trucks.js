const {Truck} = require("../../models/truckModel");
const {User} = require("../../models/userModel");


module.exports.validateIsTruckAssigned = async (req, res, next) => {
    const user = await User.findOne({_id: req.user._id})
    const created_by = user._id
    const _id = req.params['id']

    const truck = await Truck.findOne({created_by, _id}, {__v: 0});
    if(truck.assigned_to !== null) {
        return res.status(400).json({message: `'You can not delete assigned truck`})
    }

    next()
}
