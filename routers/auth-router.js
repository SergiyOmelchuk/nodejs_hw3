const express = require('express')
const router = express.Router()

const {asyncWrapper} = require('./helpers')
const {validateRegistration, validateLoginFields, validateForgotPassFields} = require('./middlewares/validation-middlewares')
const {login, registration, forgotPass} = require('../controllers/auth-controller')

router.post('/register', asyncWrapper(validateRegistration), asyncWrapper(registration))
router.post('/login', asyncWrapper(validateLoginFields), asyncWrapper(login))
router.post('/forgot_password', asyncWrapper(validateForgotPassFields), asyncWrapper(forgotPass))

module.exports = router
