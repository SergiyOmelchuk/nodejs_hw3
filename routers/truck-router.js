const express = require('express')

const router = express.Router()
const {validateAddTruckFields} = require("./middlewares/validation-middlewares")
const {validateIsTruckAssigned} = require("./middlewares/validate-assigned-trucks")
const {validateIsDriver} = require("./middlewares/validate-user-role")
const {asyncWrapper} = require('./helpers')
const {validateToken} = require('./middlewares/validate-token')
const { getTrucks, addTruck, getTruckById, deleteTruckById, updateTruckById, assignTruckById} = require('../controllers/trucks-controller')

router.post('/', asyncWrapper(validateToken), asyncWrapper(validateIsDriver), asyncWrapper(validateAddTruckFields),
    asyncWrapper(addTruck))
router.get('/', asyncWrapper(validateToken), asyncWrapper(validateIsDriver), asyncWrapper(getTrucks))
router.get('/:id', asyncWrapper(validateToken), asyncWrapper(validateIsDriver), asyncWrapper(getTruckById))
router.put('/:id', asyncWrapper(validateToken), asyncWrapper(updateTruckById))
router.delete('/:id', asyncWrapper(validateToken), asyncWrapper(validateIsDriver), asyncWrapper(validateIsTruckAssigned), asyncWrapper(deleteTruckById))
router.post('/:id/assign', asyncWrapper(validateToken), asyncWrapper(assignTruckById))


module.exports = router
