const mongoose = require('mongoose')

const userSchema = mongoose.Schema({
    email: {
        type: String,
        require: true,
        uniq: true
    },
    password: {
        type: String,
        require: true
    },
    //TODO нужно чтоб роль выбиралась только из двух вариантов
    role: {
        type: String,
        require: true,
        uppercase: true,
        enum: ['DRIVER', 'SHIPPER']
    },
    created_date: {
        type: Date,
        default: Date.now()
    }
})

module.exports.User = mongoose.model('User', userSchema)
