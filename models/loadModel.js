const mongoose = require('mongoose')
const Schema = mongoose.Schema

const loadSchema = new Schema({
    created_date: {
        type: Date,
        default: Date.now(),
    },
    created_by: {
        ref: 'users',
        type: Schema.Types.ObjectId
    },
    assigned_to: {
        ref: 'users',
        type: Schema.Types.ObjectId,
        default: null
    },
    status: {
        type: String,
        enum: [ 'NEW', 'POSTED', 'ASSIGNED', 'SHIPPED' ],
        default: 'NEW'
    },
    state: {
        type: String,
        enum: [ 'En route to Pick Up', 'Arrived to Pick Up', 'En route to delivery', 'Arrived to delivery' ],
        default: 'En route to Pick Up'
    },
    name: String,
    payload: {
        type: Number,
        min: 0
    },
    pickup_address: String,
    delivery_address: String,
    dimensions:	{
        width: Number,
        length:	Number,
        height:	Number
    },
    logs:	[{
        message: String,
        time:	{
            type: Date,
            default: Date.now()
        }
}]
})

module.exports.Load = mongoose.model('loads', loadSchema)
