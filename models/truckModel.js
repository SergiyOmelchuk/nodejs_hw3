const mongoose = require('mongoose')
const Schema = mongoose.Schema

const truckSchema = new Schema({
    created_date: {
        type: Date,
        default: Date.now(),
    },
    created_by: {
        ref: 'users',
        type: Schema.Types.ObjectId
    },
    assigned_to: {
        ref: 'users',
        type: Schema.Types.ObjectId,
        default: null
    },
    type: {
        type: String,
        enum: [ 'SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT' ]
    },
    status: {
        type: String,
        enum: [ 'OL', 'IS' ],
        default: 'IS'
    }
})

module.exports.Truck = mongoose.model('trucks', truckSchema)
